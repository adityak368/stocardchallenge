#Stocard Challenge

Steps to run
1. Clone the repo
2. npm install
3. npm start

Endpoints:

http://localhost:3000/api/v1/cities?lat={latitude}&lng={longitude}

http://localhost:3000/api/v1/cities/{city_id}

http://localhost:3000/api/v1/cities/{city_id}/weather

To run tests:

1. npm test

To Dockerize:

1. docker image build --label=Stocard .
2. docker run --name stocard-dev -p 3000:3000 -d <imageid>