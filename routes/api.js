const express = require('express');
const citiesRouter = require('./cities');

const api = express.Router();

api.use('/cities', citiesRouter);

module.exports = api;
