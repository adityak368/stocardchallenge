const express = require('express');
const router = express.Router();

router.route('/')
    .get((req, res) => {
        res.status(404).json({
            "code":"NotFoundError",
            "message":"Api Not Found",
        });
    });
  
module.exports = router;
