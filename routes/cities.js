

const express = require('express');
const router = express.Router();

const request = require('request-promise');
const cityValidator = require('../validation/city');
const config = require('../config.json');

router.route('/')
    .get(cityValidator.validate('list-cities'), async (req, res) => {
        try {

            const { lat, lng } = req.query;
            const data = await request({
                uri: `http://api.openweathermap.org/data/2.5/find?lat=${lat}&lon=${lng}&cnt=10&appid=${config.AppId}`,
                json: true,
            });
            const cities = data.list.map(city => ({
                id: city.id,
                name: city.name,
            }))
            res.status(200).json(cities);

        } catch (err) {
            res.status(500).json({
                code: 'InternalServerError',
                message:'Something went wrong',
            });
        }
    });

router.route('/:cityId')
    .get(cityValidator.validate('city-details'), async (req, res) => {
        try {
            const { cityId } = req.params;
            const weather = await request({
                uri: `http://api.openweathermap.org/data/2.5/weather?id=${cityId}&appid=${config.AppId}`,
                json: true,
            });
            res.status(200).json({
                id: weather.id,
                name: weather.name,
                lat: weather.coord.lat,
                lng: weather.coord.lon,
            });
        } catch (err) {
            if (err.name === 'StatusCodeError' && err.statusCode === 404) {
                return res.status(404).json({
                    code: 'NotFoundError',
                    message:'not found',
                });
            }
            res.status(500).json({
                code: 'InternalServerError',
                message:'Something went wrong',
            });
        }
    });

router.route('/:cityId/weather')
    .get(cityValidator.validate('city-weather'), async (req, res) => {
        try {

            const { cityId } = req.params;
            const weather = await request({
                uri: `http://api.openweathermap.org/data/2.5/weather?id=${cityId}&appid=${config.AppId}`,
                json: true,
            });
            res.status(200).json({
                'type': weather.weather[0].main,
                'type_description': weather.weather[0].description,
                'sunrise': new Date(weather.sys.sunrise),
                'sunset': new Date(weather.sys.sunset),
                ...weather.main,
                'clouds_percent': weather.clouds.all,
                'wind_speed': weather.wind.speed,
            });

        } catch (err) {
            if (err.name === 'StatusCodeError' && err.statusCode === 404) {
                return res.status(404).json({
                    code: 'NotFoundError',
                    message:'not found',
                });
            }
            res.status(500).json({
                code: 'InternalServerError',
                message:'Something went wrong',
            });
        }
    });
  
module.exports = router;
