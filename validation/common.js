const { validationResult } = require('express-validator');

const errorFormatter = ({
    location, msg, param, value, nestedErrors,
}) => ({ [param]: msg });

exports.CheckErrors = (req, res, next) => {
    const errors = validationResult(req).formatWith(errorFormatter);
    if (!errors.isEmpty()) {
        return res.status(400).json({ code: "BadRequestError", message: errors.array() });
    }
    next();
};