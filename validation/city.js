const { query, param } = require('express-validator');
const { CheckErrors } = require('../validation/common');

const validationRoutes = method => {
    switch (method) {
        case 'list-cities':
            return [
                query('lat', 'Please enter a valid latitude.').isFloat(),
                query('lng', 'Please enter a valid longitude.').isFloat(),
            ];
        case 'city-details':
            return [
                param('cityId', 'Please enter a valid City Id.').isInt(),
            ];
        case 'city-weather':
            return [
                param('cityId', 'Please enter a valid City Id.').isInt(),
            ];
        default: return [];
    }
};

exports.validate = method => validationRoutes(method).concat(CheckErrors);