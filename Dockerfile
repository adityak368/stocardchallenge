# specify the node base image with your desired version node:<version>
FROM node:latest
# replace this with your application's default port
EXPOSE 3000

# Create app directory
WORKDIR /usr/src/StocardChallenge

# Copy app source
COPY . .

# Install app dependencies
RUN npm install
# If you are building your code for production
# RUN npm install --only=production


CMD [ "npm", "start" ]