const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const data = require('./data.json');

chai.use(chaiHttp);
chai.should();

describe("Api Tests", () => {
    describe("GET /api/v1/cities", () => {

        it("List all cities near the coordinates with invalid coordinates", (done) => {
            const lat = 49.48;
            const lng = "asd";
            chai.request(app)
                .get(`/api/v1/cities?lat=${lat}&lng=${lng}`)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('code');
                    res.body.should.have.property('message');
                    res.body.code.should.equal('BadRequestError');
                    done();
                });
        });

        it("List all cities near the coordinates", (done) => {
            const lat = 49.48;
            const lng = 8.46;
             chai.request(app)
                .get(`/api/v1/cities?lat=${lat}&lng=${lng}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    //res.body.should.equal(data.cityList);
                    done();
                });
        });

        it("List details of a city with valid id", (done) => {
            const id = 2873891;
            chai.request(app)
                .get(`/api/v1/cities/${id}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    //res.body.should.equal(data.cityDetails);
                    done();
                });
        });

        it("List weather of a city with valid id", (done) => {
            const id = 2873891;
            chai.request(app)
                .get(`/api/v1/cities/${id}/weather`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    //res.body.should.equal(data.cityWeather);
                    done();
                });
        });

        it("List details of a city with invalid id", (done) => {
            const id = 123564;
            chai.request(app)
                .get(`/api/v1/cities/${id}`)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('code');
                    res.body.should.have.property('message');
                    res.body.code.should.equal('NotFoundError');
                    res.body.message.should.equal('not found');
                    done();
                });
        });

        it("List weather of a city with invalid id", (done) => {
            const id = 123564;
            chai.request(app)
                .get(`/api/v1/cities/${id}/weather`)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('code');
                    res.body.should.have.property('message');
                    res.body.code.should.equal('NotFoundError');
                    res.body.message.should.equal('not found');
                    done();
                });
        });
    });
});